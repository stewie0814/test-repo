# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Bitbucket::Application.config.secret_key_base = 'b1a3651d56875dfd32255ca3d490c4e449f6fb0473d9a147f6f8b4fe2e446ab1e5c4af5f5c868b8fbb4618561f57e490a4dadca704546a3aa2683639a5759bf5'
